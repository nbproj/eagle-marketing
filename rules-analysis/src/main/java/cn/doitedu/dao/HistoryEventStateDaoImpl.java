package cn.doitedu.dao;

import cn.doitedu.pojo.CombineCondition;
import cn.doitedu.pojo.EventCondition;
import cn.doitedu.pojo.EventStateBean;
import cn.doitedu.pojo.LogBean;
import org.apache.flink.api.common.state.ListState;
import org.apache.flink.api.java.utils.ParameterTool;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class HistoryEventStateDaoImpl implements HistoryEventDao {

    private ListState<EventStateBean> eventsState;

    public HistoryEventStateDaoImpl(ListState<EventStateBean> eventsState) {
        this.eventsState = eventsState;
    }




    @Override
    public String queryEventSequenceStr(LogBean bean, CombineCondition combineCondition) throws Exception {

        //查询条件
        long startTime = combineCondition.getStartTime();
        long endTime = combineCondition.getEndTime();
        //预先设置的条件（期望匹配的规则）
        List<String> targets = combineCondition.getEventConditions().stream().map(EventCondition::getEventId).collect(Collectors.toList());
        StringBuffer sb = new StringBuffer();
        Iterable<EventStateBean> events = eventsState.get();
        for (EventStateBean event : events) {
            long timeStamp = event.getTimeStamp();
            if (timeStamp >= startTime && timeStamp <= endTime) {
                //将事件映射成短的数字或字母
                sb.append(targets.indexOf(event.getEventId()) + 1);
            }
        }
        return sb.toString();
    }
}
