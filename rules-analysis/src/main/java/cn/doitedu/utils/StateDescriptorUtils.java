package cn.doitedu.utils;

import cn.doitedu.pojo.RulesBean;
import org.apache.flink.api.common.state.MapStateDescriptor;

public class StateDescriptorUtils {


  public static final MapStateDescriptor<Long, RulesBean>  rulesStateDescriptor =  new MapStateDescriptor<Long, RulesBean>("rules-state-descriptor", Long.class, RulesBean.class);

}
