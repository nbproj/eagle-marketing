package cn.doitedu.cache;

import java.util.Set;

public interface CacheManager {


    String getData(String bigKey, String smallKey);

    void setValue(String bigKey, String smallKey, String value);

    void setValueEx(String bigKey, String smallKey, String value, long ttl);

    Set<String> getTimeRanges(String bigKey);
}
