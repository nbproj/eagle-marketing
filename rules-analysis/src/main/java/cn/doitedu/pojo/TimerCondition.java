package cn.doitedu.pojo;

import lombok.Data;

//用了封装时间触发的规则
@Data
public class TimerCondition {

    private Long timerRuleId;

    //延迟触发的时间
    private Long lateTime;

    //可以配置统一的条件（可以查行为次数，也可以查行为序列）
    private CombineCondition combineCondition;
}
