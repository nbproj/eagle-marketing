package cn.doitedu.pojo;

import com.alibaba.fastjson.JSON;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/***
 * 运行一次，生成一条行为日志
 *
 *  /bigdata/kafka_2.12-2.6.2/bin/kafka-topics.sh --create --zookeeper node-1.51doit.cn:2181,node-2.51doit.cn:2181,node-3.51doit.cn:2181 --replication-factor 3 --partitions 4 --topic eagle-applog
 **/
public class ActionLogGenOne {

    public static void main(String[] args) {

        Properties props = new Properties();
        props.setProperty("bootstrap.servers", "node-1.51doit.cn:9092,node-2.51doit.cn:9092,node-3.51doit.cn:9092");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        KafkaProducer<String, String> kafkaProducer = new KafkaProducer<>(props);

        LogBean logBean = new LogBean();
        logBean.setDeviceId("000053");
        logBean.setEventId("E");
        Map<String, String> ps = new HashMap();
        props.put("p1", "v1");
        logBean.setProperties(ps);
        logBean.setTimeStamp(System.currentTimeMillis());

        String log = JSON.toJSONString(logBean);
        ProducerRecord<String, String> record = new ProducerRecord<>("eagle-applog", log);
        kafkaProducer.send(record);
        kafkaProducer.flush();
    }
}